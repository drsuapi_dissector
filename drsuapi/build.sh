#!/bin/bash

ws_src=~/work/wireshark-svn
caps_dir=/home/kamen/work/net-drs/logs

echo "0. backup..."
cp packet-dcerpc-drsuapi.c packet-dcerpc-drsuapi.c.bak
cp packet-dcerpc-drsuapi.h packet-dcerpc-drsuapi.h.bak

echo "1. compile drsuapi.idl"
make || exit 1
if [ "$1" == "1" ]; then
	exit 0
fi

echo "2. copy generated dissector"
cp packet-dcerpc-drsuapi.* "$ws_src/epan/dissectors/"

echo "3. build wireshark"
pushd "$ws_src"
make
res=$?
popd
# check build result
[ $res == 0 ] || exit $res

echo "4. run wireshar to test ..."
# w2k3 -> w2k8
#"$ws_src/wireshark" -K $caps_dir/samba.devel/krb5.kt $caps_dir/samba.devel/repadmin-showrepl-2.cap
# w2k8 <-> w2k8
#"$ws_src/wireshark" -K $caps_dir/samba.devel_2dc/krb5.kt $caps_dir/samba.devel_2dc/repadmin-bind.cap
# w2k3 <-> w2k3
#"$ws_src/wireshark" -K $caps_dir/saas.kdimitrov.local/krb5.kt $caps_dir/saas.kdimitrov.local/repadmin-kcc-1.cap
# w2k8 <-> w2k8 32bit
"$ws_src/wireshark" -K $caps_dir/w2k8-32.devel/krb5.kt $caps_dir/w2k8-32.devel/repadmin-syncall.cap
